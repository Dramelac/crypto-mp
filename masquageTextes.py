#3	DISSIMULATION DE TEXTES
import pygame.surfarray

def buildArrayImg(filename):     #chargement de l'image défini
    image = pygame.image.load(filename)
    return pygame.surfarray.array3d(image)

def traitementSurface(l, h, table):
    for x in range(l):
        for y in range(h):
            for z in range(3):
                if table[x][y][z] == 255:
                    table[x][y][z] = 254
    return table

def conversionBinaire(text, long):  #Converti chaque lettre du texte à coder en binaire en 1 octet
    liste = [[0]*8 for i in range(long)]
    for i in range(len(text)):
        temp = bin(ord(text[i]))
        p = 10-len(temp)
        for y in range(2, len(temp)):
            liste[i][p] = int(temp[y])
            p += 1
    return liste

def conversionText(liste):  #convertion du binaire vers le decimal
    char = 0
    indice = 0
    for i in range(7, -1, -1):
        if liste[indice]:
            char += 2 ** i
        indice += 1
    return char

def traitementTexte(l, h, FileName, resultFileName, texte):  #chiffrement de limage
    File = traitementSurface(l, h, buildArrayImg(FileName))
    liste = conversionBinaire(texte, len(texte))
    indiceTexte = 0
    indiceBit = 0
    for x in range(l):
        for y in range(h):
            for z in range(3):
                if File[x][y][z] % 2 == 0 and liste[indiceTexte][indiceBit]:
                    File[x][y][z] += 1
                elif not File[x][y][z] % 2 == 0 and not liste[indiceTexte][indiceBit]:
                    File[x][y][z] += 1
                indiceBit += 1
                if indiceBit >= 8:
                    indiceTexte += 1
                    indiceBit = 0
                    if indiceTexte >= len(liste):
                        surface = pygame.surfarray.make_surface(File)
                        pygame.image.save(surface, resultFileName)
                        return

def texteRetour(l,h,FileName):
    File = traitementSurface(l, h, buildArrayImg(FileName))
    liste = [[0]*8 for i in range(len(File)**2*3//8)]
    indiceTexte = 0
    indiceBit = 0
    for x in range(l):
        for y in range(h):
            for z in range(3):
                if not File[x][y][z] % 2 == 0:
                    liste[indiceTexte][indiceBit] = 1
                indiceBit += 1
                if indiceBit >= 8:
                    print(chr(conversionText(liste[indiceTexte])), end='')
                    indiceTexte += 1
                    indiceBit = 0

l = eval(input("largeur = "))
h = eval(input("hauteur = "))

booleen = eval(input("Crypter ou decrypter ? 1 / 0 : "))

if booleen:
    texte = input("texte = ")
    FileName = input("Nom du fichier à crypter : ")
    resultFileName = "crypter.bmp"
    traitementTexte(l, h, FileName, resultFileName, texte)
else:
    FileName = input("Nom du fichier à decrypter : ")
    resultFileName = "derypter.bmp"
    texteRetour(l, h, FileName)