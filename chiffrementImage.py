#1	CHIFFREMENT D’IMAGES
import pygame.surfarray, random, sys

def buildArrayImg(filename):    #chargement de l'image défini
    image = pygame.image.load(filename)
    return pygame.surfarray.array3d(image)

def XOR(a,b):
    if (a + b) == 1:
        return True
    return False

def conversionBinaire(x): #converti en binaire
    temp = bin(x)
    binaire = [0]*8
    p = 10-len(temp)
    for i in range(2,len(temp)):
        binaire[p] = int(temp[i])
        p += 1
    return binaire

def soustractionBinaire(a, b):  #XOR binaire et traitement du resultat vers le decimal
    resultat = 0
    indice = 0
    binA = conversionBinaire(a)
    binB = conversionBinaire(b)
    for i in range(7, -1, -1):
        if XOR(binA[indice], binB[indice]):
            resultat += 2**i
        indice += 1
    return resultat

def tuplesXOR(t_uple1, t_uple2): #remplissage du t_uple apres XOR binaire
    temp = [0]*3
    for x in range(3):
        temp[x] = soustractionBinaire(t_uple1[x], t_uple2[x])
    return (temp[0], temp[1], temp[2])

def generateRandom(l, h, file): #définition d'une image aléatoir de longeur l et de hauteur h avec un RGB aléatoir
    pygame.init()
    image = pygame.display.set_mode((l, h))
    for x in range(l):
        for y in range(h):
            image.set_at((x, y), ([random.randrange(0,256), random.randrange(0,256), random.randrange(0,256)]))
    pygame.image.save(image, file)

def cryptage(l, h, FileName, keyFileName, resultFileName): #cryptage de l'image
    File = buildArrayImg(FileName)
    keyFile = buildArrayImg(keyFileName)
    resultFile = pygame.display.set_mode((l, h))
    for x in range(l):
        for y in range(h):
            resultFile.set_at((x, y), tuplesXOR(File[x][y], keyFile[x][y]))
    pygame.image.save(resultFile, resultFileName)

l = eval(input("largeur = "))
h = eval(input("hauteur = "))

booleen = eval(input("Crypter ou decrypter ? 1 / 0 : "))

if booleen:
    FileName = input("Nom du fichier à crypter (sans extention) : ")

    keyFileName = FileName + "Key" + ".bmp"
    generateRandom(l, h, keyFileName)

    resultFileName = FileName + "Crypter.bmp"
    FileName += ".bmp"
    print("Cryptage en cours ...")
    cryptage(l, h, FileName, keyFileName, resultFileName)

else:
    FileName = input("Nom du fichier à décrypter (sans extention) : ")
    keyFileName = input("Nom de la clé de crtyptage (avec extention) : ")
    resultFileName = FileName + "Decrypter.bmp"
    FileName += ".bmp"
    print("Décryptage en cours ...")
    cryptage(l, h, FileName, keyFileName, resultFileName)