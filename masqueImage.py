#2	DISSIMULATION D’IMAGES
import pygame.surfarray, sys

def buildArrayImg(filename):
    image = pygame.image.load(filename)
    return pygame.surfarray.array3d(image)

def choixCryptDecrypt(n, m, op, k): #choix de l'action (cryptage / déchiffrage) en fonction de OP
    if op == 1:
        result = n+(1/k)*m
        if result > 255:
            return 255
        return result
    else:
        return k*(m-n)

def t_UplSolution(op, k, t_upl1, t_upl2): #définir le RGB du t_upl solution
    temp = [0]*3
    for i in range (3):
        temp[i] = choixCryptDecrypt(t_upl1[i], t_upl2[i], op, k)
    return (temp[0], temp[1], temp[2])

def cryptage(l, h, FileName, keyFileName, resultFileName, k, op): #cryptage de l'image
    File = buildArrayImg(FileName)
    keyFile = buildArrayImg(keyFileName)
    resultFile = pygame.display.set_mode((l, h))

    for x in range(l):             #parcours de l'image
        for y in range(h):
            resultFile.set_at((x, y), t_UplSolution(op, k, keyFile[x][y], File[x][y]))

    pygame.image.save(resultFile, resultFileName)   #sauvegarde de l'image crypter

l = eval(input("largeur = "))
h = eval(input("hauteur = "))
k = eval(input("clé paramètre =  "))

op = eval(input("Crypter ou decrypter ? 1 / 0 : "))

keyFileName = input("Nom du fichier clé : ")

if op:    #Instruction en fonction de la valeur OP
    op = 1
    FileName = input("Nom du fichier à crypter : ")

    resultFileName = "crypter.bmp"
    print("Cryptage en cours ...")
    cryptage(l, h, FileName, keyFileName, resultFileName, k ,op)
else:
    FileName = input("Nom du fichier à decrypter : ")

    resultFileName = "derypter.bmp"
    print("Decryptage en cours ...")
    cryptage(l, h, FileName, keyFileName, resultFileName, k ,op)